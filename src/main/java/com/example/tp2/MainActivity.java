package com.example.tp2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;

import java.util.HashMap;


//TODO : find where the position is given as an id and replace it by position+1 to get the right id to update
public class MainActivity extends AppCompatActivity {

    public static WineDbHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView listview = (ListView) findViewById(R.id.listViewWines);
        //final String[][] wines = new String[][] {{"Châteauneuf-du-Pape", "Vallée du Rhône"}, {"Arbois", "Jura"}, {"Beaumes-de-Venise", "Vallée du Rhône"}, {"Bergerac", "Sud-Ouest"}, {"Côte-de-Brouilly", "Beaujolais"}, {"Muscadet", "Vallée de la Loire"}, {"Bandol", "Provence"}, {"Vouvray", "Touraine"}};

        /*Wine[] wines = new Wine[] {new Wine("Châteauneuf-du-Pape", "Vallée du Rhône"),
                new Wine("Arbois", "Jura"),
                new Wine("Beaumes-de-Venise", "Vallée du Rhône"),
                new Wine("Bergerac", "Sud-Ouest"),
                new Wine("Côte-de-Brouilly", "Beaujolais"),
                new Wine("Muscadet", "Vallée de la Loire"),
                new Wine("Bandol", "Provence"),
                new Wine("Vouvray", "Touraine")};*/
        //final WineAdapter adapter = new WineAdapter(this, wines);

        //this.deleteDatabase("wine.db");
        final WineDbHelper wineDbHelper = new WineDbHelper(this);
        MainActivity.helper = wineDbHelper;
        //wineDbHelper.populate();
        Cursor cursor = wineDbHelper.fetchAllWines();
        //wineDbHelper.deleteWineWithName("Nouveau vin");
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.wine_row, cursor, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION, WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA, WineDbHelper._ID}, new int[]{R.id.name, R.id.domain}, 0);
        listview.setAdapter(adapter);
        registerForContextMenu(listview);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listview = (ListView) findViewById(R.id.listViewWines);
                Cursor c = (Cursor) listview.getItemAtPosition(position);
                Wine clicked = WineDbHelper.cursorToWine(c);
                clicked.setId(position + 1);
                Intent switchToWine = new Intent(MainActivity.this, WineActivity.class);
                switchToWine.putExtra("wine", clicked);
                startActivity(switchToWine);
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // add wine
                Snackbar.make(view, "Ajouter un nouveau vin", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Wine newWine = new Wine("Nouveau vin", "", "", "", "");
                boolean done = MainActivity.helper.addWine(newWine);
                if(done) {
                    finish();
                    startActivity(getIntent());
                    Intent switchToWine = new Intent(MainActivity.this, WineActivity.class);
                    switchToWine.putExtra("wine", newWine);
                    startActivity(switchToWine);
                    Toast.makeText(MainActivity.this, "marche", Toast.LENGTH_SHORT).show();
                }
                else alertExistingWine();
            }
        });

    }

    public void alertExistingWine() {
        AlertDialog.Builder existingAlert = new AlertDialog.Builder(this)
                .setTitle("Ajout impossible.")
                .setMessage("Un autre vin avec le même nom existe déjà dans notre base de données.");
        existingAlert.create().show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ListView listview = (ListView) findViewById(R.id.listViewWines);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int index = info.position;
        Cursor c = (Cursor) listview.getItemAtPosition(index);
        Wine clicked = WineDbHelper.cursorToWine(c);
        Toast.makeText(this, "Le vin "+clicked.getTitle()+" va être supprimé", Toast.LENGTH_SHORT).show();
        MainActivity.helper.deleteWine(c);
        Intent switchToWine = new Intent(MainActivity.this, MainActivity.class);
        startActivity(switchToWine);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*public class WineAdapter extends ArrayAdapter<Wine> {
        public WineAdapter(Context context, Wine[] wines) {
            super(context, 0, wines);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Wine wine = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.wine_row, parent, false);
            }
            // Lookup view for data population
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView domain = (TextView) convertView.findViewById(R.id.domain);
            // Populate the data into the template view using the data object
            name.setText(wine.getTitle());
            domain.setText(wine.getRegion());
            // Return the completed view to render on screen
            return convertView;
        }
    }*/
}
