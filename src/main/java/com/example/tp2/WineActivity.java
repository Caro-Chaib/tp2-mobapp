package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class WineActivity extends AppCompatActivity {

    public static long currentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        Wine wine = (Wine) getIntent().getExtras().get("wine");
        WineActivity.currentId = wine.getId();

        // Fill view with clicked wine info
        EditText name = (EditText) findViewById(R.id.wineName);
        name.setText(wine.getTitle(), TextView.BufferType.EDITABLE);

        EditText region = (EditText) findViewById(R.id.editWineRegion);
        region.setText(wine.getRegion());

        EditText loc = (EditText) findViewById(R.id.editLoc);
        loc.setText(wine.getLocalization());

        EditText climate = (EditText) findViewById(R.id.editClimate);
        climate.setText(wine.getClimate());

        EditText planted = (EditText) findViewById(R.id.editPlantedArea);
        planted.setText(wine.getPlantedArea());

        // Save button
        Button save = (Button) findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText name = (EditText) findViewById(R.id.wineName);
                String newName = name.getText().toString();

                EditText region = (EditText) findViewById(R.id.editWineRegion);
                String newRegion = region.getText().toString();

                EditText loc = (EditText) findViewById(R.id.editLoc);
                String newLoc = loc.getText().toString();

                EditText climate = (EditText) findViewById(R.id.editClimate);
                String newClimate = climate.getText().toString();

                EditText planted = (EditText) findViewById(R.id.editPlantedArea);
                String newPlanted = planted.getText().toString();

                Wine newWine = new Wine(WineActivity.currentId, newName, newRegion, newLoc, newClimate, newPlanted);
                int res = MainActivity.helper.updateWine(newWine);
                if(res == -1) alertEmptyName();
                else if(res == -2) alertExistingWine();
                else {
                    Intent switchToMenu = new Intent(WineActivity.this, MainActivity.class);
                    switchToMenu.putExtra("wine", newWine);
                    startActivity(switchToMenu);
                }
            }
        });

    }

    public void alertExistingWine() {
        AlertDialog.Builder existingAlert = new AlertDialog.Builder(this)
                .setTitle("Ajout impossible.")
                .setMessage("Un autre vin avec le même nom existe déjà dans notre base de données.");
        existingAlert.create().show();
    }

    public void alertEmptyName() {
        AlertDialog.Builder emptyAlert = new AlertDialog.Builder(this)
                .setTitle("Sauvegarde impossible.")
                .setMessage("Le nom du vin ne peut pas être vide.");
        emptyAlert.create().show();
    }


}
