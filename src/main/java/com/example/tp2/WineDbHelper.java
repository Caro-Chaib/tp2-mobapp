package com.example.tp2;

// access db : device file explorer > data > data > com.example.tp2

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public static long id = 0;

    public Context context;

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        WineDbHelper.id = this.getCount();
    }


    //TODO
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE cellar (" + _ID + " NUMERIC, "+COLUMN_NAME+" TEXT, "+COLUMN_WINE_REGION+" TEXT, "+COLUMN_LOC+" TEXT, "+COLUMN_CLIMATE+" TEXT, "+COLUMN_PLANTED_AREA+" TEXT, UNIQUE (" + COLUMN_NAME + ", " + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //TODO
   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        long rowId = 0;
        ContentValues cv=new ContentValues();
        cv.put(_ID, WineDbHelper.id);
        WineDbHelper.id = WineDbHelper.id+1;
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        rowId = db.insert(TABLE_NAME, null, cv);

        db.close(); // Closing database connection

        if(this.context != null) {
            if (rowId == -1) {
                Toast.makeText(this.context, "Le vin n'a pas pu être ajouté", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this.context, "Le vin demandé a bien été ajouté à notre base", Toast.LENGTH_LONG).show();
            }
        }

        return (rowId != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        if(existsWine(wine)) return -2;
        if(!wine.getTitle().equals("")) {
            SQLiteDatabase db = this.getWritableDatabase();
            int res = 0;
            ContentValues cv = new ContentValues();
            cv.put(_ID, wine.getId());
            cv.put(COLUMN_NAME, wine.getTitle());
            cv.put(COLUMN_WINE_REGION, wine.getRegion());
            cv.put(COLUMN_LOC, wine.getLocalization());
            cv.put(COLUMN_CLIMATE, wine.getClimate());
            cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
            res = db.update(TABLE_NAME, cv, _ID + "=" + wine.getId(), null);
            db.close();
            return res;
        }
        return -1;
    }

    public boolean existsWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean exists = false;
        Cursor res = db.rawQuery("select * from "+TABLE_NAME+" where name = '"+wine.getTitle()+"' and region = '"+wine.getRegion()+"';", null);
        boolean result = res.getCount() != 0;
        db.close();
        return(result);
    }

    //TODO
    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();
        final Cursor cursor = db.rawQuery("SELECT * FROM cellar;", null);
        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        long id = cursorToWine(cursor).getId();
        db.delete(TABLE_NAME, _ID + "=" + id, null);
        db.close();
    }

     public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));


        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(_ID));
        String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        String region = cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION));
        String loc = cursor.getString(cursor.getColumnIndex(COLUMN_LOC));
        String climate = cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE));
        String planted = cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA));
        Wine wine = new Wine(id, name, region, loc, climate, planted);
        return wine;
    }

    public long getCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        return numRows+1;
    }
}
